package com.igor.market.Products;

import com.igor.market.productAbstract.Vegetable;

public class Tomato extends Vegetable {
    public Tomato(String name, int price, String maker, int count) {
        super(name, price, maker, count);
    }

    public int getWeight() {
        return this.weight;
    }

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price * this.weight;
    }

    public String getMaker() {
        return this.maker;
    }

    public int getExpirationDate() {
        return this.expirationDate;
    }

    @Override
    public String toString() {
        return "Tomato{" +
                "weight=" + weight +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", maker='" + maker + '\'' +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
