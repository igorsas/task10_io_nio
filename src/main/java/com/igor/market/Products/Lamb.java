package com.igor.market.Products;

import com.igor.market.productAbstract.Meat;

public class Lamb extends Meat {
    public Lamb(String name, int price, String maker, int weight) {
        super(name, price, maker, weight);
    }

    public int getWeight() {
        return this.weight;
    }

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price * this.weight;
    }

    public String getMaker() {
        return this.maker;
    }

    public int getExpirationDate() {
        return this.expirationDate;
    }

    @Override
    public String toString() {
        return "Lamb{" +
                "weight=" + weight +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", maker='" + maker + '\'' +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
