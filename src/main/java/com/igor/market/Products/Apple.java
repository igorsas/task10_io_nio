package com.igor.market.Products;

import com.igor.market.productAbstract.Fruit;

public class Apple extends Fruit {
    public Apple(String name, int price, String maker, int count) {
        super(name, price, maker, count);
    }

    @Override
    public String toString() {
        return "Apple{" +
                "count=" + count +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", maker='" + maker + '\'' +
                ", expirationDate=" + expirationDate +
                '}';
    }

    public int getCount() {
        return this.count;
    }

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price * this.count;
    }

    public String getMaker() {
        return this.maker;
    }

    public int getExpirationDate() {
        return this.expirationDate;
    }

}
