package com.igor.market.Products;

import com.igor.market.productAbstract.Dairy;

public class Milk extends Dairy {
    public Milk(String name, int price, String maker, int percentOfFat) {
        super(name, price, maker, percentOfFat);
    }

    public int getPercentOfFat() {
        return this.percentOfFat;
    }

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price;
    }

    public String getMaker() {
        return this.maker;
    }

    public int getExpirationDate() {
        return this.expirationDate;
    }

    @Override
    public String toString() {
        return "Milk{" +
                "percentOfFat=" + percentOfFat +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", maker='" + maker + '\'' +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
