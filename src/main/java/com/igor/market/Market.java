package com.igor.market;

import com.igor.market.productAbstract.Product;

import java.io.Serializable;
import java.util.List;

public class Market implements Serializable {
    private String name;
    private int yearsOfWork;
    private List<Product> products;
    private transient int percentageOfProfit;

    public Market(String name, int yearsOfWork, List<Product> products, int percentageOfProfit) {
        this.name = name;
        this.yearsOfWork = yearsOfWork;
        this.products = products;
        this.percentageOfProfit = percentageOfProfit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Market{" +
                "name='" + name + '\'' +
                ", yearsOfWork=" + yearsOfWork +
                ", products=" + products +
                ", percentageOfProfit=" + percentageOfProfit +
                '}';
    }

    public int getYearsOfWork() {
        return yearsOfWork;
    }

    public void setYearsOfWork(int yearsOfWork) {
        this.yearsOfWork = yearsOfWork;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getPercentageOfProfit() {
        return percentageOfProfit;
    }

    public void setPercentageOfProfit(int percentageOfProfit) {
        this.percentageOfProfit = percentageOfProfit;
    }
}
