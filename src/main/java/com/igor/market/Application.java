package com.igor.market;

import com.igor.market.Products.Apple;
import com.igor.market.Products.Lamb;
import com.igor.market.Products.Milk;
import com.igor.market.Products.Tomato;
import com.igor.market.productAbstract.Product;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void test(Logger logger) {
        Market market = new Market("ATB", 20, getProductsArray(), 2);
        try {
            writeObject(market);
            Market marketFromFile = readObject();
            logger.info(marketFromFile.toString());
        } catch (IOException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        }


    }

    public static void writeObject(Market market) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("market.dat"));
        out.writeObject(market);
        out.close();
    }

    public static Market readObject() throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("market.dat"));
        Market marketFromFile = (Market) in.readObject();
        in.close();
        return marketFromFile;
    }

    private static List<Product> getProductsArray() {
        Apple apple = new Apple("redApple", 10, "Garden", 3);
        Lamb lamb = new Lamb("MetroLamb", 100, "Metro", 1);
        Tomato tomato = new Tomato("SvoiaLiniaTomato", 30, "SvoiaLinia", 5);
        Milk milk = new Milk("Milk", 15, "VillageInc", 2);
        ArrayList<Product> products = new ArrayList<Product>(4);
        products.add(apple);
        products.add(lamb);
        products.add(tomato);
        products.add(milk);
        return products;
    }
}
