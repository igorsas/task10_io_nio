package com.igor.market.productAbstract;

import java.io.Serializable;

public abstract class Product implements Serializable {
    protected String name;
    protected int price;
    protected String maker;
    protected int expirationDate;

    protected Product(String name, int price, String maker) {
        this.name = name;
        this.price = price;
        this.maker = maker;
    }

    public abstract String getName();

    public abstract int getPrice();

    public abstract String getMaker();

    public abstract int getExpirationDate();
}
