package com.igor.market.productAbstract;

public abstract class Meat extends Product {
    protected int weight;

    protected Meat(String name, int price, String maker, int weight) {
        super(name, price, maker);
        this.weight = weight;
    }

    public abstract int getWeight();
}
