package com.igor.market.productAbstract;

public abstract class Fruit extends Product {
    protected int count;

    protected Fruit(String name, int price, String maker, int count) {
        super(name, price, maker);
        this.count = count;
    }

    public abstract int getCount();
}
