package com.igor.market.productAbstract;

public abstract class Dairy extends Product {
    protected int percentOfFat;

    protected Dairy(String name, int price, String maker, int percentOfFat) {
        super(name, price, maker);
        this.percentOfFat = percentOfFat;
    }

    public abstract int getPercentOfFat();
}
