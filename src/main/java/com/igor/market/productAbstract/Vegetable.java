package com.igor.market.productAbstract;

public abstract class Vegetable extends Product {
    protected int weight;

    protected Vegetable(String name, int price, String maker, int weight) {
        super(name, price, maker);
        this.weight = weight;
    }

    public abstract int getWeight();
}
