package com.igor.ReadSourceCode;

import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class Application {
    private Logger logger;

    public Application(Logger logger) {
        this.logger = logger;
        start();
    }

    public void start() {
        try {
            BufferedReader stream = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(
                                    new File("C:\\" +
                                            "Users\\igor_\\" +
                                            "IdeaProjects\\" +
                                            "task10_io_nio\\" +
                                            "src\\main\\" +
                                            "resources\\" +
                                            "BufferedReader.java"))));
            readComents(stream);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void readComents(BufferedReader stream) throws IOException {
        List<String> lines = stream.lines().collect(Collectors.toList());
        int startedLine = -1;
        int finishedLine;
        int indexStarted = -1;
        int indexFinished;
        for (int i = 0; i < lines.size(); i++) {

            if (indexStarted == -1) {
                indexStarted = hasStartedComentSign(lines.get(i));
                if (indexStarted != -1) {
                    startedLine = i;
                }
            }
            indexFinished = hasFinishedComentSign(lines.get(i));
            if (indexStarted == -1 || indexFinished == -1) {
                continue;
            } else {
                finishedLine = i;
                writeInfo(lines, indexStarted, indexFinished, startedLine, finishedLine);
            }
        }
    }

    private void writeInfo(List<String> lines, int indexStarted, int indexFinished, int startedLine, int finishedLine) {
        if (startedLine == finishedLine) {
            logger.info(lines.get(startedLine).substring(indexStarted, indexFinished));
        } else {
            StringBuilder coment = new StringBuilder(lines.get(startedLine).substring(indexStarted));
            for (int i = startedLine + 1; i < finishedLine; i++) {
                coment.append("\n").append(lines.get(i));
            }
            coment.append("\n").append(lines.get(finishedLine), 0, indexFinished);
            logger.info(coment.toString());
        }
    }

    private int hasStartedComentSign(String line) {
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '/') {
                i++;
                if (i < line.length() && line.charAt(i) == '*') {
                    return i - 1;
                }
            }
        }
        return -1;
    }

    private int hasFinishedComentSign(String line) {
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '*') {
                i++;
                if (i < line.length() && line.charAt(i) == '/') {
                    return i + 1;
                }
            }
        }
        return -1;
    }
}