package com.igor.FileManager;

import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Objects;


public class ReadDirectory {
    private static Logger logger;
    private static final String dir = "C:\\Users\\igor_\\OneDrive\\\\Desktop";

    private static void displayAllFiles(String directoryName){
        File directory = new File(directoryName);
        File[] files = directory.listFiles();
        for (File file : Objects.requireNonNull(files)){
            if (file.isFile()){
                logger.info(file.getAbsolutePath());
            } else if (file.isDirectory()){
                displayAllFiles(file.getAbsolutePath());
            }
        }
    }

    private static void displayFilesAndFolders(){
        File directory = new File(ReadDirectory.dir);

        File[] fileList = directory.listFiles();
        for (File file : Objects.requireNonNull(fileList)){
            logger.info(file.getName());
        }
        logger.debug("Finished! Displayed all files from " + ReadDirectory.dir);
    }

    public static void start(Logger logger) {
        ReadDirectory.logger = logger;
        logger.debug("All: ");
        ReadDirectory.displayAllFiles(dir);
        logger.debug("All files and folders: ");
        ReadDirectory.displayFilesAndFolders();
    }
}