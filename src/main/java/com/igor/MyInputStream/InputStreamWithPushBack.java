package com.igor.MyInputStream;

import java.io.InputStream;
import java.io.PushbackInputStream;

public class InputStreamWithPushBack extends PushbackInputStream {
    public InputStreamWithPushBack(InputStream in, int size) {
        super(in, size);
    }

    public InputStreamWithPushBack(InputStream in) {
        super(in);
    }
}
