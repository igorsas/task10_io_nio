package com.igor.MyInputStream;

import org.apache.logging.log4j.Logger;

import java.io.*;

public class InputStreamRunner {
    public static void test(Logger logger) {
        try {
            String text = new BufferedReader(new InputStreamReader(new FileInputStream("text.txt"))).readLine();
            byte[] bytes = text.getBytes();
            InputStreamWithPushBack reader = new InputStreamWithPushBack(new ByteArrayInputStream(bytes), bytes.length);

            byte[] readedBytes = reader.readAllBytes();
            printInfo(readedBytes, logger);

            reader.unread(readedBytes);
            logger.info("\nUnread bytes");
            readedBytes = reader.readAllBytes();
            printInfo(readedBytes, logger);

            reader.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void printInfo(byte[] bytes, Logger logger) {
        logger.info("\nRead bytes");
        for (byte readedByte : bytes) {
            logger.info(readedByte);
        }
    }
}
