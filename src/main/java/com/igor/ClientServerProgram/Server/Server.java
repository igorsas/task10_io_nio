package com.igor.ClientServerProgram.Server;

import java.io.*;
import java.net.Socket;

class Server extends Thread {

    private Socket socket; //сокет для зв'язування клієнтів з сервером
    private BufferedReader in;
    private BufferedWriter out;


    public Server(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        ServerRunner.story.printStory(out); //виводимо останні 10 повідомлень з сокета
        start();
    }

    @Override
    public void run() {
        String word;
        try {
            // перше повідомлення буде ніком користувача
            word = in.readLine();
            try {
                out.write(word + "\n");
                out.flush(); // flush() необхідний для виштовхування даних, що залишилися, або чистки потоку
            } catch (IOException ignored) {
            }
            try {
                while (true) {
                    word = in.readLine();
                    if (word.equals("stop")) {
                        this.downService(); // харакірі
                        break;
                    }
                    System.out.println("Message: " + word);
                    ServerRunner.story.addStoryEl(word);
                    for (Server vr : ServerRunner.serverList) {
                        vr.send(word); // розшарити повідомлення
                    }
                }
            } catch (NullPointerException ignored) {
            }


        } catch (IOException e) {
            this.downService();
        }
    }

    /**
     * поширити повідомлення
     *
     * @param msg
     */
    private void send(String msg) {
        try {
            out.write(msg + "\n");
            out.flush(); //очищаємо буфер
        } catch (IOException ignored) {
        }

    }

    /**
     * закриваємо сервер
     */
    private void downService() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
                for (Server vr : ServerRunner.serverList) {
                    if (vr.equals(this)) vr.interrupt();
                    ServerRunner.serverList.remove(this);
                }
            }
        } catch (IOException ignored) {
        }
    }
}
