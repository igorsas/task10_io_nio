package com.igor.ClientServerProgram;

import com.igor.ClientServerProgram.Client.Clients;
import com.igor.ClientServerProgram.Server.ServerRunner;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {
    public static void runClientServerProgram(Logger logger) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            logger.debug("Choose program for calling: " +
                    "\n1 - Run server" +
                    "\n2 - Run first client" +
                    "\n3 - Run second client" +
                    "\n4 - Run third client" +
                    "\nq - Quit");
            String point = "q";
            try {
                point = reader.readLine();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
            try {
                switch (point) {
                    case "1":
                        ServerRunner.main(new String[]{"Run server"});
                        break;
                    case "2":
                        Clients.Client1.main(new String[]{"Run server"});
                        break;
                    case "3":
                        Clients.Client1.main(new String[]{"Run server"});
                        break;
                    case "4":
                        Clients.Client1.main(new String[]{"Run server"});
                        break;
                    default:
                        return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
