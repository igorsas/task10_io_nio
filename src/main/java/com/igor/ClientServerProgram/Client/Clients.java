package com.igor.ClientServerProgram.Client;

import java.io.IOException;

public class Clients {
    private static String ipAddr = "localhost";
    private static int port = 8080;

    public static class Client1 {
        public static void main(String[] args) throws IOException {
            new Client(ipAddr, port);
        }
    }

    public static class Client2 {
        public static void main(String[] args) throws IOException {
            new Client(ipAddr, port);
        }
    }

    public static class Client3 {
        public static void main(String[] args) throws IOException {
            new Client(ipAddr, port);
        }
    }
}
