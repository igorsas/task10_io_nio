package com.igor.ClientServerProgram.Client;

import java.io.BufferedReader;
import java.net.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Client {

    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    private BufferedReader inputUser; //зчитати з консолі
    private String nickname;

    public Client(String addr, int port) throws IOException {
        this.socket = new Socket(addr, port);
        try {
            inputUser = new BufferedReader(new InputStreamReader(System.in));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.pressNickname();
            new ReadMsg().start(); // вічно читає повідомлення з сервера
            new WriteMsg().start(); // вічно пише повідомлення, що вводять в консолі, на сервер
        } catch (IOException e) {
            Client.this.downService();
        }
    }

    /**
     * Введення імені користувача
     */

    private void pressNickname() {
        System.out.print("Press your nick: ");
        try {
            nickname = inputUser.readLine();
            out.write("Hello " + nickname + "\n");
            out.flush();
        } catch (IOException ignored) {
        }

    }

    /**
     * закрити сокет
     */
    private void downService() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
            }
        } catch (IOException ignored) {
        }
    }

    // читання повідомлень з сервера
    private class ReadMsg extends Thread {
        @Override
        public void run() {

            String str;
            try {
                while (true) {
                    str = in.readLine(); // чекаємо повідомлення з сервера
                    if (str.equals("stop")) {
                        Client.this.downService(); // харакірі
                        break;
                    }
                    System.out.println(str);
                }
            } catch (IOException e) {
                Client.this.downService();
            }
        }
    }

    // запис повідомлень, що вводять в консолі, на сервер
    public class WriteMsg extends Thread {

        @Override
        public void run() {
            while (true) {
                String userWord;
                try {
                    userWord = inputUser.readLine(); // сообщения с консоли
                    if (userWord.equals("stop")) {
                        out.write("stop" + "\n");
                        Client.this.downService(); // харакири
                        break;
                    } else {
                        out.write(nickname + ": " + userWord + "\n"); //відправляємо на сервер
                    }
                    out.flush(); // чистим
                } catch (IOException e) {
                    Client.this.downService(); // в случае исключения тоже харакири

                }

            }
        }
    }
}