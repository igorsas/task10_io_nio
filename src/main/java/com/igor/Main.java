package com.igor;

import com.igor.ComparingUsualAndBufferingReader.Application;
import com.igor.FileManager.ReadDirectory;
import com.igor.MyInputStream.InputStreamRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            logger.debug("Choose program for calling: " +
                    "\n1 - Comparing usual and buffered reader " +
                    "\n2 - Serializable test" +
                    "\n3 - Test MyInputStream with push back" +
                    "\n4 - Read coments from the class" +
                    "\n5 - Run client-server program" +
                    "\n6 - Read directory path" +
                    "\nq - Quit");
            String point = "q";
            try {
                point = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (point) {
                case "1":
                    Application.compare(logger);
                    break;
                case "2":
                    com.igor.market.Application.test(logger);
                    break;
                case "3":
                    InputStreamRunner.test(logger);
                    break;
                case "4":
                    new com.igor.ReadSourceCode.Application(logger);
                    break;
                case "5":
                    com.igor.ClientServerProgram.Application.runClientServerProgram(logger);
                case "6":
                    ReadDirectory.start(logger);
                default:
                    return;
            }
        }
    }
}
