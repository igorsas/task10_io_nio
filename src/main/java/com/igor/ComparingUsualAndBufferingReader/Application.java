package com.igor.ComparingUsualAndBufferingReader;

import org.apache.logging.log4j.Logger;

import java.io.*;

public class Application {
    public static void compare(Logger logger) {
        try {
            long start = System.currentTimeMillis();
            InputStream usualReader = new FileInputStream
                    ("c:\\Users\\igor_\\Downloads\\file.pdf");
            int data = usualReader.read();
            while (data != -1) {
                data = usualReader.read();
            }
            usualReader.close();
            long end = System.currentTimeMillis();
            logger.info("Read file with 9.59 MB by usual reader in seconds: " + (end - start) / 1000);
            start = System.currentTimeMillis();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream("c:\\Users\\igor_\\Downloads\\file.pdf")));

            bufferedReader.read();
            end = System.currentTimeMillis();
            logger.info("Read file with 9.59 MB by buffered reader in seconds: " + (end - start) / 1000);


        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }


}
